# Spring Boot Application - README

This README file provides instructions on how to run a ``inventoryService`` stored in GitLab. Follow the steps below to get started.
This service keeps track of inventory items.

## Prerequisites

Before running the application, ensure that you have the following installed:

- Java Development Kit (JDK) 17 LTS or higher
- Maven build tool
- Git

## Clone the Repository

1. Open a terminal or command prompt.
2. Clone the `https://gitlab.com/learnly2/inventoryservice.git` repository using the `git clone` command:


## Build the Application

1. Navigate to the project directory:
2. Build the application using Maven and execute the following command `mvn -Dspring.profiles.active=dev package`


This command compiles the source code, runs tests, and creates an executable JAR file.

## Run the Application

1. After the build process completes successfully, navigate to target directory `cd target`
2. Run the following command `java -Dspring.profiles.active=dev -jar inventory-service.jar` to run the app
3. The application will start, and you should see logs indicating its progress. The `inventoryService` will run on port `5003` so make sure no other process is running on the same port

**Note** In case you need to `Dockerize` the app run the following command `docker build --build-arg APP_ENV="dev" --tag="ph-inventory-service" .` from the project root directory.

## Access the Application

Once the application is running, you can access it using a web browser or an API testing tool. By default, the application runs on `http://localhost:5003`.
You can also access the Swagger API documentation on `http://localhost:5003/swagger/webjars/swagger-ui/index.html#/`

## Configuration
This service require access to `POSTGRESQL Database` and database named `ph_inventoryservice` need to be created before running the app.
If there's need to modify the database configuration, modify the `application-dev.properties`  file located in the project's resources directory.

For authentication purpose, this service require access to running and configured [keycloak identity service](https://www.keycloak.org/). The instructions on how to configure and install keycloak is provided in `authClient`.
In `dev / local` environment, These are the endpoints provided by keycloak 
```
spring.security.oauth2.resourceserver.jwt.issuer-uri=http://0.0.0.0:8080/realms/pharmacy
spring.security.oauth2.resourceserver.jwt.jwk-set-uri=http://0.0.0.0:8080/realms/pharmacy/protocol/openid-connect/certs
```

This service require access to `RabbitMQ` message broker to receive inventory adjustment message from `financeService`. The instructions on how to run `RabbitMQ` is provided in deployment folder `README.md`.
In `dev / local` environment, These are the rabbitMQ configuration properties in `application-dev.properties`
```
spring.rabbitmq.host=localhost
spring.rabbitmq.port=5672
spring.rabbitmq.username=guest
spring.rabbitmq.password=guest
spring.rabbitmq.dynamic=true
```

**Note** The `database name` cannot be changed, but you can change the `username` & `password`. It's also important to maintain the port number as `5003` as required by the `gatewayService` for routing purpose.

## Conclusion

You have successfully cloned, built, and run the Spring Boot application stored in GitLab. In case of any challenge please reach out.
