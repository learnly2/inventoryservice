CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

SELECT uuid_generate_v1mc();

CREATE TABLE IF NOT EXISTS inventory
(
    id        varchar(100)   not null primary key DEFAULT uuid_generate_v1mc(),
    name      varchar(200)   not null,
    quantity  NUMERIC(10, 0) not null,
    price     NUMERIC        not null,
    exempt    boolean        not null,
    createdAt timestamp      not null,
    updatedAt timestamp      not null
);
