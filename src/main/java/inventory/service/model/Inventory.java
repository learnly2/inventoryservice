package inventory.service.model;

import inventory.service.model.audit.DateAudit;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import java.math.BigDecimal;

@Table("inventory")
public class Inventory extends DateAudit {
    @Id
    @Column("id")
    private String id;

    @Column("name")
    private String name;

    @Column("quantity")
    private int quantity;

    @Column("price")
    private BigDecimal price;

    @Column("exempt")
    private boolean exempt = false;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public boolean isExempt() {
        return exempt;
    }

    public void setExempt(boolean exempt) {
        this.exempt = exempt;
    }
}
