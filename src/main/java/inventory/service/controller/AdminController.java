package inventory.service.controller;

import inventory.service.model.Inventory;
import inventory.service.payload.AddInventoryRequest;
import inventory.service.payload.DeleteItemRequest;
import inventory.service.service.InventoryServiceImpl;
import io.swagger.v3.oas.annotations.Operation;
import jakarta.validation.Valid;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping(value = "/api/v1/inventory", produces = {"application/json; charset=utf-8"}, consumes = {"application/json"})
@PreAuthorize("hasRole('ROLE_ADMIN')")
public class AdminController {

    final InventoryServiceImpl inventoryService;

    public AdminController(InventoryServiceImpl inventoryService) {
        this.inventoryService = inventoryService;
    }

    @Operation(summary = "Fetch all inventory items")
    @GetMapping
    public Flux<Inventory> getAllActive() {
        return inventoryService.getAll();
    }

    @Operation(summary = "Add inventory item")
    @PostMapping
    public Mono<Inventory> add(@Valid @RequestBody AddInventoryRequest request) {
        return inventoryService.add(
                request.inventoryId(),
                request.name(),
                request.quantity(),
                request.price()
        );
    }

    @Operation(summary = "Soft delete item")
    @DeleteMapping
    public Mono<String> deleteItem(@Valid @RequestBody DeleteItemRequest request) {
        return inventoryService
                .deleteRecord(request.inventoryId())
                .map(Inventory::getId);
    }
}
