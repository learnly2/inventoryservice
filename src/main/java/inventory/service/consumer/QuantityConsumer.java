package inventory.service.consumer;

import com.fasterxml.jackson.core.type.TypeReference;
import inventory.service.message.InventoryAdjustment;
import inventory.service.message.InventoryQuantityMessage;
import inventory.service.model.Inventory;
import inventory.service.service.InventoryServiceImpl;
import inventory.service.util.JacksonUtil;
import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;
import reactor.rabbitmq.Receiver;
import java.util.List;

@Component
public class QuantityConsumer {
    private final Receiver receiver;
    private final JacksonUtil jacksonUtil;
    private final InventoryServiceImpl inventoryService;

    public QuantityConsumer(
            Receiver receiver,
            JacksonUtil jacksonUtil,
            InventoryServiceImpl inventoryService
    ) {
        this.receiver = receiver;
        this.jacksonUtil = jacksonUtil;
        this.inventoryService = inventoryService;
    }

    @PostConstruct
    private void init() {
        consume().subscribe();
    }

    //TODO capture message if the consumer throws an exception
    public Mono<Void> consume() {
        return receiver.consumeAutoAck("QUANTITY_QUEUE")
                .flatMap(m -> Mono
                        .fromCallable(() -> jacksonUtil.deserializeObject(new String(m.getBody()), new TypeReference<InventoryQuantityMessage>() {
                        }))
                        .flatMap(i -> reduceInventory(i.items()).then(Mono.empty()))
                )
                .parallel()
                .runOn(Schedulers.boundedElastic())
                .sequential()
                .then(Mono.empty());
    }

    Flux<Inventory> reduceInventory(List<InventoryAdjustment> items) {
        return Flux.fromIterable(items)
                .flatMapSequential(i -> inventoryService.reduceQuantity(i.inventoryId(), i.quantity()));
    }

    @PreDestroy
    public void close() {
        receiver.close();
    }
}

