package inventory.service.payload;

import jakarta.validation.constraints.NotBlank;

public record DeleteItemRequest(@NotBlank(message = "Item reference ID is required!") String inventoryId) {
}
