package inventory.service.payload;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

import java.math.BigDecimal;

public record AddInventoryRequest(
        String inventoryId,
        @NotBlank(message = "Item name is required") String name,
        @NotNull(message = "Quantity cannot be null") int quantity,
        @NotNull(message = "Price cannot be null") BigDecimal price
) {
}
