package inventory.service.repository;

import inventory.service.model.Inventory;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;

public interface InventoryRepository extends ReactiveCrudRepository<Inventory, String> {
    Flux<Inventory> getAllByExemptFalse();
}
