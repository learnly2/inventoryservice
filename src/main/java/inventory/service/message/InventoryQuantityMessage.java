package inventory.service.message;

import java.util.List;

public record InventoryQuantityMessage(List<InventoryAdjustment> items) {
}
