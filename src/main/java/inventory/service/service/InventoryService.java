package inventory.service.service;

import inventory.service.model.Inventory;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.math.BigDecimal;

public interface InventoryService {
    Mono<Inventory> add(String inventoryId, String name, int quantity, BigDecimal price);

    Flux<Inventory> getAll();

    Mono<Inventory> getOne(String inventoryId);

    Mono<Inventory> getOptional(String inventoryId);

    Mono<Inventory> deleteRecord(String inventoryId);

    Mono<Inventory> reduceQuantity(String inventoryId, int quantity);
}
