package inventory.service.service;

import inventory.service.exception.DataViolationException;
import inventory.service.exception.NotFoundException;
import inventory.service.model.Inventory;
import inventory.service.repository.InventoryRepository;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.math.BigDecimal;

@Service
public class InventoryServiceImpl implements InventoryService {

    final InventoryRepository repository;

    public InventoryServiceImpl(InventoryRepository repository) {
        this.repository = repository;
    }

    @Override
    public Mono<Inventory> add(String inventoryId, String name, int quantity, BigDecimal price) {
        return getOptional(inventoryId)
                .map(i -> {
                    i.setName(name);
                    i.setPrice(price);
                    i.setQuantity(quantity);
                    return i;
                })
                .flatMap(repository::save)
                .onErrorMap(ex -> new DataViolationException("DVE-001", ex));
    }

    @Override
    public Flux<Inventory> getAll() {
        return repository.getAllByExemptFalse();
    }

    @Override
    public synchronized Mono<Inventory> getOne(String inventoryId) {
        return repository
                .findById(inventoryId)
                .switchIfEmpty(Mono.error(new NotFoundException("NFE-001")));
    }

    @Override
    public Mono<Inventory> getOptional(String inventoryId) {
        return repository
                .findById(inventoryId)
                .switchIfEmpty(Mono.just(new Inventory()));
    }

    @Override
    public Mono<Inventory> deleteRecord(String inventoryId) {
        return getOne(inventoryId)
                .map(i -> {
                    i.setExempt(true);
                    return i;
                })
                .flatMap(repository::save)
                .onErrorMap(ex -> new DataViolationException("DVE-001", ex));
    }

    @Override
    public synchronized Mono<Inventory> reduceQuantity(String inventoryId, int quantity) {
        return getOne(inventoryId)
                .map(i -> {
                    i.setQuantity(i.getQuantity() - quantity);
                    return i;
                })
                .flatMap(repository::save)
                .onErrorMap(ex -> new DataViolationException("DVE-001", ex));
    }
}
