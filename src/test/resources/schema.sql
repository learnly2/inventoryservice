CREATE TABLE IF NOT EXISTS inventory
(
    id        varchar(100)   not null primary key,
    name      varchar(200)   not null,
    quantity  NUMERIC(10, 0) not null,
    price     NUMERIC        not null,
    exempt    boolean        not null,
    createdAt timestamp      not null,
    updatedAt timestamp      not null
);