package inventory.service.service;

import inventory.service.exception.DataViolationException;
import inventory.service.model.Inventory;
import inventory.service.repository.InventoryRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;

@ExtendWith(MockitoExtension.class)
@ExtendWith(SpringExtension.class)
class InventoryServiceImplTest {

    @Mock
    InventoryRepository repository;

    @InjectMocks
    InventoryServiceImpl inventoryService;

    Inventory mockInventory;

    @BeforeEach
    void setUp() {
        inventoryService = new InventoryServiceImpl(repository);
        mockInventory = new Inventory();
        mockInventory.setQuantity(2);
        mockInventory.setPrice(BigDecimal.valueOf(200));
        mockInventory.setExempt(false);
        mockInventory.setName("AstraZeneca");
        mockInventory.setId("astra_1");
    }

    @AfterEach
    void tearDown() {
        mockInventory = null;
    }

    @Test
    void addInventoryTest() {
        Mockito.when(repository.save(any(Inventory.class)))
                .thenReturn(Mono.just(mockInventory));

        Mono<Inventory> inventoryMono = repository.save(mockInventory);

        StepVerifier
                .create(inventoryMono)
                .expectNext(mockInventory)
                .verifyComplete();
    }

    @Test
    void getAllTest() {
        List<Inventory> mockInventories = new ArrayList<>();
        mockInventories.add(mockInventory);

        Mockito.when(inventoryService.getAll()).thenReturn(Flux.fromIterable(mockInventories));
        Flux<Inventory> inventoryFlux = inventoryService.getAll();

        StepVerifier
                .create(inventoryFlux)
                .expectNext(mockInventories.get(0))
                .verifyComplete();
    }

    @Test
    void getOneTest() {
        Mockito.when(repository.findById("astra_1")).thenReturn(Mono.just(mockInventory));

        Mono<Inventory> inventoryMono = inventoryService.getOne("astra_1");

        StepVerifier
                .create(inventoryMono)
                .assertNext(inventory -> {
                    assertEquals(2, inventory.getQuantity());
                    assertEquals(BigDecimal.valueOf(200), inventory.getPrice());
                    assertFalse(inventory.isExempt());
                    assertEquals("AstraZeneca", inventory.getName());
                    assertEquals("astra_1", inventory.getId());
                })
                .verifyComplete();
    }

    @Test
    void getOptionalTest() {
        Mockito.when(repository.findById("astra_1")).thenReturn(Mono.just(mockInventory));
        Mono<Inventory> inventoryMono = inventoryService.getOptional("astra_1");

        StepVerifier
                .create(inventoryMono)
                .expectNext(mockInventory)
                .expectComplete()
                .verify();

        Mockito.when(repository.findById("astra_12")).thenReturn(Mono.empty());

        Mono<Inventory> emptyInventory = inventoryService.getOptional("astra_12");

        StepVerifier
                .create(emptyInventory)
                .expectNextMatches(Objects::nonNull)
                .expectComplete()
                .verify();
    }

    @Test
    void deleteRecordTest() {
        Mockito.when(repository.findById("astra_1"))
                .thenReturn(Mono.just(mockInventory));

        Mockito.when(repository.save(any(Inventory.class)))
                .thenReturn(Mono.just(mockInventory));

        Mono<Inventory> emptyInventory = inventoryService.deleteRecord("astra_1");

        StepVerifier
                .create(emptyInventory)
                .assertNext(inventory -> {
                    assertEquals(2, inventory.getQuantity());
                    assertEquals(BigDecimal.valueOf(200), inventory.getPrice());
                    assertTrue(inventory.isExempt());
                    assertEquals("AstraZeneca", inventory.getName());
                    assertEquals("astra_1", inventory.getId());
                })
                .verifyComplete();

        Mockito.when(repository.findById("astra_123"))
                .thenReturn(Mono.empty());

        Mono<Inventory> exceptionInventory = inventoryService.deleteRecord("astra_123");

        StepVerifier.create(exceptionInventory)
                .expectError(DataViolationException.class)
                .verify();
    }
}