package inventory.service.repository;

import inventory.service.model.Inventory;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.r2dbc.DataR2dbcTest;
import org.springframework.data.r2dbc.core.R2dbcEntityTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

import java.math.BigDecimal;

@DataR2dbcTest
@TestPropertySource(
        locations = "classpath:application.properties")
@ActiveProfiles("test")
class InventoryRepositoryTest {

    @Autowired
    private R2dbcEntityTemplate template;

    @Autowired
    private InventoryRepository inventoryRepository;

    Inventory testInventory;

    @BeforeEach
    void setUp() {
        testInventory = new Inventory();
        testInventory.setQuantity(2);
        testInventory.setPrice(BigDecimal.valueOf(200));
        testInventory.setExempt(false);
        testInventory.setName("AstraZeneca");
        testInventory.setId("astra_1");

        template.insert(testInventory).block();
    }

    @AfterEach
    void tearDown() {
        template.delete(testInventory).block();
        testInventory = null;
    }

    @Test
    void testGetAllByExemptFalse() {
        Flux<Inventory> inventoryFlux = inventoryRepository.getAllByExemptFalse();

        StepVerifier
                .create(inventoryFlux)
                .expectNextCount(1)
                .verifyComplete();
    }
}